from django.db import models
from django.contrib.auth.models import AbstractUser,BaseUserManager
class User(AbstractUser):
    GENDER = (
       ('M', 'Male'),
       ('F', 'Female'),
       ('NI', 'Ni')
    )

    nombre = models.CharField(max_length=50)
    apellido = models.CharField(max_length=50)
    fecNacimiento = models.DateField(null=True)
    genero = models.CharField(max_length=2,choices=GENDER)
