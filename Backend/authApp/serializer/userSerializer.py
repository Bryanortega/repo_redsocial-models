from rest_framework import serializers
from authApp.models.user import User

class UserSerializer(serializers.Serializer):
    id = serializers.ReadOnlyField()
    nombre = serializers.CharField()
    apellido = serializers.CharField()
    fecNacimiento = serializers.DateField()
    genero = serializers.ChoiceField(choices=User.GENDER)
    username = serializers.CharField()
    email = serializers.EmailField()
    password = serializers.CharField()

    def create(self, validated_data):
        instance = User()
        instance.nombre = validated_data.get('nombre')
        instance.apellido = validated_data.get('apellido')
        instance.fecNacimiento = validated_data.get('fecNacimiento')
        instance.genero = validated_data.get('genero')
        instance.username = validated_data.get('username')
        instance.email = validated_data.get('email')
        instance.set_password(validated_data.get('password'))
        instance.save()
        return instance
    
    def validate_username(self,data):
        users = User.objects.filter(username=data)
        if len(users) != 0:
            raise serializers.ValidationError("El usuario ya esta registrado, ingrese uno nuevo")
        else:
            return data