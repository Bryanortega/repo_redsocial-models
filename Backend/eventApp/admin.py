from django.contrib import admin
from .models.event import Event

# Register your models here.

class EventAdmin(admin.ModelAdmin):
    list_per_page = 20
    list_display = ('place', 'event_name', 'start_date', 'final_date', 'is_active')
    search_fields = ['event_name']
    list_filter = ('place', 'start_date', 'is_active')


admin.site.register(Event, EventAdmin)
