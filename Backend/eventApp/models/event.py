from django.db import models

class Event(models.Model):
    id =  models.AutoField(primary_key=True)
    place = models.CharField(max_length=50, null=True)
    event_name = models.CharField(max_length=50, null=True)
    start_date = models.DateField(null=True)
    final_date = models.DateField(null=True)
    start_time = models.TimeField(null=True)
    final_time = models.TimeField(null=True)
    description = models.TextField()
    is_active = models.BooleanField(default=True)
