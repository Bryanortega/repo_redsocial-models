from rest_framework import serializers
from eventApp.models.event import Event

class EventSerializer(serializers.ModelSerializer):
    class Meta:
        model = Event
        fields = ['place', 'event_name', 'start_date', 'final_date', 'start_time', 'final_time', 'is_active']