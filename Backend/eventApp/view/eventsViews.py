from rest_framework import views, status
from rest_framework.response import Response
from eventApp.models import Event
from eventApp.serializer.eventSerializer import EventSerializer
from rest_framework import generics


#List all accounts and Create an event
class EventListCreateView(generics.ListCreateAPIView):
    queryset = Event.objects.all()
    serializer_class = EventSerializer


# Retrieve, Update, Delete an event
class EventRetrieveUpdateDeleteView(generics.RetrieveUpdateDestroyAPIView):
    queryset = Event.objects.all()
    serializer_class = EventSerializer
