from django.contrib import admin

# Register your models here.

from .models.pub import Publication
from .models.tag import Tag

admin.site.register(Publication)
admin.site.register(Tag)