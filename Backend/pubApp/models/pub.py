from django.db import models
from authApp.models.user import User
from .tag import Tag

class Publication(models.Model):
    id =  models.AutoField(primary_key=True)
    account_id = models.ForeignKey(User,related_name="Publication",on_delete = models.CASCADE)
    tags = models.ManyToManyField(Tag)
    place = models.CharField(max_length=50, blank = False)
    date = models.DateField()
    description = models.TextField(max_length=250)

    def __str__(self):
        return self.description


