from django.db import models

class Tag(models.Model):
    id =  models.AutoField(primary_key=True)
    name = models.CharField(max_length=50)
    description = models.TextField(blank = True)
    is_active = models.BooleanField(default=True)

    def __str__(self):
        return self.name