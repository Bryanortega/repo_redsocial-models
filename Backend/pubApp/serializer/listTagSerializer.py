from rest_framework import serializers
from pubApp.models.tag import Tag

class ListTagserializer(serializers.ModelSerializer):
    class Meta:
        model = Tag
        fields = ('name',)