from rest_framework import serializers
from pubApp.models.pub import Publication

class PublicationSerializer(serializers.ModelSerializer):
    class Meta:
        model = Publication
        fields = ['account_id', 'tags', 'place', 'date', 'description']