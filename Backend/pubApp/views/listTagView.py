from eventApp.models import Event
from pubApp.serializer.listTagSerializer import ListTagserializer
from rest_framework.generics import ListAPIView
from pubApp.models.tag import Tag


class LisTagView(ListAPIView):
    serializer_class = ListTagserializer

    def get_queryset(self):
        return Tag.objects.all()
