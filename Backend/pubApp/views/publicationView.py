from rest_framework import views, status
from rest_framework.response import Response
from pubApp.models.pub import Publication
from pubApp.serializer.publicationSerializer import PublicationSerializer
from rest_framework import generics



class PublicationListCreateView(generics.ListCreateAPIView):
    queryset = Publication.objects.all()
    serializer_class = PublicationSerializer


class PublicationRetrieveUpdateDeleteView(generics.RetrieveUpdateDestroyAPIView):
    queryset = Publication.objects.all()
    serializer_class = PublicationSerializer
